import React from "react";
import PropTypes from 'prop-types';
import classes from "./button.module.scss";
import cx from 'classnames';

export const ButtonStyles = {
    small: 'small',
    blue: 'blue',
    darkBlue: 'darkBlue',
    gray: 'gray',
    lightGray: 'lightGray',
    circle: 'circle',
    arrowLeft: 'arrowLeft',
    arrowRight: 'arrowRight',
    fullWidth: 'fullWidth',
    blueBorder: 'blueBorder',
    rounded: 'rounded',
    white: 'white',
    whiteBorder: 'whiteBorder',
    shadow: 'shadow',
    noBorder: 'noBorder'
};

const Button = ({children, handler, styles = [], large = false, disabled = false}) => {
    const styleClasses = {};
    styles.forEach((style) => {
       styleClasses[classes[style]] = true;
    });
    return (
        <button className={cx({[classes.button]: true, [classes.large]: large, ...styleClasses})} onClick={disabled ? () => null : handler}>{children}</button>
    );
}

Button.propTypes = {
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
    handler: PropTypes.func.isRequired,
    large: PropTypes.bool,
    styles: PropTypes.arrayOf(PropTypes.oneOf(Object.values(ButtonStyles))),
    disabled: PropTypes.bool

}

export default Button;