import React from "react";
import classes from "./compare.module.scss";

const Compare = () => {
    return (
        <div className={classes.screen}>
            <div className={classes.compare}>
                <div>Сравни цены и сервис.Почувствуй разницу Сравни цены и сервис.Почувствуй разницу</div>
                <div>Сравни цены и сервис.Почувствуй разницу <span>Сравни цены и</span> сервис.Почувствуй разницу</div>
                <div>Сравни цены и <span>сервис.Почувствуй</span> разницу Сравни цены и сервис.Почувствуй разницу</div>
                <div>Сравни цены и сервис.Почувствуй <span>разницу</span> Сравни цены и сервис.Почувствуй разницу</div>
                <div>Сравни цены и сервис.Почувствуй разницу Сравни цены и сервис.Почувствуй разницу</div>
            </div>
            <div className={classes.agcIs}>
                <div className={classes.logo}></div>
                <div className={classes.service} data-type={'plan'}>
                    <div className={classes.logo}/>
                    <div className={classes.title}>Плановое обслуживание</div>
                </div>
                <div className={classes.service} data-type={'prepare'}>
                    <div className={classes.logo}/>
                    <div className={classes.title}>Подготовка автомобиля к сезону</div>
                </div>
                <div className={classes.service} data-type={'wheel'}>
                    <div className={classes.logo}/>
                    <div className={classes.title}>Диагностика и ремонт ходовой части</div>
                </div>
                <div className={classes.service} data-type={'guard'}>
                    <div className={classes.logo}/>
                    <div className={classes.title}>Охрана автомобиля</div>
                </div>

            </div>
        </div>
    );
}

export default Compare;