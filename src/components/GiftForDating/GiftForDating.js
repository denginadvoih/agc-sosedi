import React from 'react';
import PropTypes from 'prop-types';
import 'swiper/css';
import classes from './gift.module.scss';
import { ButtonStyles } from 'components/Button/Button';
import Button from 'components/Button';
import Close from 'components/Close';

const GiftForDating = ({ gift, close }) => {

    const breakBr = (text) => {
        const textPieces = text.split('<br>');
        return textPieces.length > 1 ? text.split('<br>').map(t => <>{t}<br/></>) : text;
    }

    return (
        <div className={classes.screen}>
            <Close className={classes.close} handler={() => close()}/>
            <div className={classes.image}>
                {gift.image ? <img src={gift.image} alt={''}/> : null}
            </div>
            <div className={classes.title}>{gift.title.split('<br>').map(t => <>{t}<br/></>)}</div>
            {[1, 2, 3].map(blockNumber =>
                <div className={classes.block} key={blockNumber}>
                    <div className={classes.texts}>
                        {[1, 2, 3].map(textNumber =>
                            <div
                                className={classes['text' + textNumber]}>{breakBr(gift['f_' + blockNumber + '_' + textNumber])}</div>
                        )}
                    </div>
                    {
                        blockNumber !== 4
                            ? <div className={classes.price}>
                                <div className={classes.text2}>0Р</div>
                                <div className={classes.notGift}>
                                    {gift['price_' + blockNumber] ? gift['price_' + blockNumber] + 'Р' : ''}
                                </div>
                            </div>
                            : null
                    }
                </div>
            )}
            <div className={classes.block4}>
                Cравни цены и сервис, почувствуй разницу
            </div>
            {gift.profit ? (
                <div className={classes.profit}>
                    <div><span>Ваша выгода</span></div>
                    <div><span>{gift.profit}</span><span>₽</span></div>
                </div>
            ) : null }
            {[4].map(blockNumber =>
                <div className={classes.block + ' ' + classes.four} key={blockNumber}>
                    <div className={classes.texts}>
                        {[1, 2, 3].map(textNumber =>
                            <div
                                className={classes['text' + textNumber]}>{breakBr(gift['f_' + blockNumber + '_' + textNumber])}</div>
                        )}
                    </div>
                </div>
            )}
            <div className={classes.button}>
                <Button handler={() => {
                    window.location.href = 'https://ortus.ru/w/2222999';
                }} styles={[ButtonStyles.white, ButtonStyles.fullWidth]}>ЗАПИСАТЬСЯ</Button>
            </div>
            <div className={classes.buttons}>
                <a className={classes.whatsapp} href="https://wa.me/79140773596">Написать</a>
                <a className={classes.phone} href="tel:+74232222999">Позвонить</a>
            </div>
        </div>
    );
}

GiftForDating.propTypes =
{
    gift: PropTypes.object
}

export default GiftForDating;