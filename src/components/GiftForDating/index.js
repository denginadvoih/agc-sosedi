import {connect} from "react-redux";
import { fetchServiceCards } from "redux/data";
import Gift from './Gift.js';

const mapStateToProps = state => {
    return {
        serviceCards: state.data.serviceCards
    }
};

export default connect(mapStateToProps, { fetchServiceCards })(Gift);