import React from 'react';
import PropTypes from 'prop-types';
import classes from './topService.module.scss';

const TopService = ({ id, title, image, final }) => {
    return final ?
        (<div className={classes.topService + ' ' + classes.final}>
            <div className={classes.image}>
                <div className={classes.text}>
                    + 108 услуг<br/>подробнее
                </div>
            </div>
        </div>)
        :
        (<div className={classes.topService} data-id={id}>
            <div className={classes.image}>
                <img src={image} alt=""/>
            </div>
            <div className={classes.textPosition}>
                <div className={classes.title}>{title}</div>
            </div>
        </div>)
};

TopService.propTypes = {
    id: PropTypes.number,
    title: PropTypes.string,
    image: PropTypes.string
}

export default TopService;