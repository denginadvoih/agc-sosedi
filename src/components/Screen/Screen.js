import React from "react";
import PropTypes from 'prop-types';
import classes from "./screen.module.scss";
import cx from 'classnames';

const Screen = ({children, active = false}) => {
    return (
        <div className={cx({[classes.screen]: true, [classes.active]: active})}>{children}</div>
    );
}

Screen.propTypes = {
    children: PropTypes.oneOfType([PropTypes.element, PropTypes.string]).isRequired,
    active: PropTypes.bool
}

export default Screen;