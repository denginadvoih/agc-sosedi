import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import classes from './footer2.module.scss';
import cx from 'classnames';
import 'swiper/css';
import Button from '../Button';
import { ButtonStyles } from '../Button/Button';

const Footer2 = () => {
    const [active, setActive] = useState(false);



    useEffect(() => {
        const clickHandler = () => setActive(false);
        document.addEventListener('click', clickHandler);
        let xDown = null;
        let yDown = null;
        function getTouches(evt) {
            return evt.touches ||             // browser API
                evt.originalEvent.touches; // jQuery
        }
        const handleTouchStart = (evt) => {
            const firstTouch = getTouches(evt)[0];
            xDown = firstTouch.clientX;
            yDown = firstTouch.clientY;
        };
        const handleTouchMove = (evt) => {
            if ( ! xDown || ! yDown ) {
                return;
            }
            var xUp = evt.touches[0].clientX;
            var yUp = evt.touches[0].clientY;
            var xDiff = xDown - xUp;
            var yDiff = yDown - yUp;
            if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
                if ( xDiff > 0 ) {
                    /* right swipe */
                } else {
                    /* left swipe */
                }
            } else {
                if ( yDiff > 0 ) {

                } else {
                    setActive(false);
                }
            }
            /* reset values */
            xDown = null;
            yDown = null;
        };
        const footer = document.getElementById('footer2');
        if (footer) {
            footer.addEventListener('touchstart', handleTouchStart, false);
            footer.addEventListener('touchmove', handleTouchMove, false);
        }
        return () => {
            footer.removeEventListener('click', clickHandler);
        }
    }, []);

    return (
        <div className={cx({ [classes.footer]: true, [classes.active]: active })} id="footer2">
            <div className={classes.wrapper}>
                <div className={classes.notActive}>
                    <Button styles={[ButtonStyles.blue, ButtonStyles.fullWidth]} handler={(e) => {
                        e.stopPropagation();
                        setActive(true);
                    }}>Заглянуть
                        в гости</Button>
                </div>
                <div className={classes.active}>
                    <Button styles={[ButtonStyles.blue, ButtonStyles.fullWidth]} handler={(e) => {
                        e.stopPropagation();
                        window.location.href = 'https://ortus.ru/w/2222999';
                    }}>Записаться</Button>
                    <Button styles={[ButtonStyles.white, ButtonStyles.fullWidth]}
                            handler={(e) => {
                                e.stopPropagation();
                                window.location.href = 'tel:+74232222999';
                            }}>
                        Позвонить
                    </Button>
                    <Button styles={[ButtonStyles.white, ButtonStyles.fullWidth]}
                            handler={(e) => {
                                e.stopPropagation();
                                window.location.href = 'https://wa.me/79140773596';
                            }}
                    >
                        НАПИСАТЬ в Whatsapp
                    </Button>
                    <Button styles={[ButtonStyles.white, ButtonStyles.fullWidth]}
                            handler={(e) => {
                                e.stopPropagation();
                                window.location.href = 'https://t.me/+79140773596';
                            }}
                    >НАПИСАТЬ в Telegram</Button>
                    <Button styles={[ButtonStyles.blue, ButtonStyles.fullWidth]} handler={
                        (e) => {
                            e.stopPropagation();
                            const element = document.getElementById('ortusScreen');
                            if (element) {
                                element.scrollIntoView();
                            }
                            setActive(false);
                        }
                    }
                    >СКАЧАТЬ ORTUS</Button>
                </div>

            </div>
        </div>
    );
}

Footer2.propTypes = {
    active: PropTypes.bool
}

export default Footer2;
