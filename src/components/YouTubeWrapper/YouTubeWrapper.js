import React, {useState} from "react";
import PropTypes from 'prop-types';
import classes from "./youtubewrapper.module.scss";
import YouTube from "react-youtube";

const YouTubeWrapper = ({id, title, preview}) => {
    const [player, setPlayer] = useState();
    const [onPlay, setOnPlay] = useState(false);
    const onPlayerReady = (event) => {
        const player = event.target;
        setPlayer(player);
    };

    const options = {
        height: "100%",
        width: "100%",
        playerVars: {
            autoplay: 0,
        },
    };

    return <div className={classes.video + ' ' + (onPlay ? classes.onPlay : '')}>
        <YouTube
            videoId={id}
            opts={options}
            onReady={onPlayerReady}
            className={classes.wrapper}

        />
        <div className={classes.preview}><img src={preview} alt={''}/></div>
        <div className={classes.title}>{title}</div>
        <div className={classes.play} onClick={() => {
        setOnPlay(true);
        player.playVideo()
        }
        }/>
    </div>
    ;
}

YouTubeWrapper.propTypes = {
    id: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
    image: PropTypes.string
}

export default YouTubeWrapper;