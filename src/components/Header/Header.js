import React, {useState} from "react";
import PropTypes from 'prop-types';
import classes from "./header.module.scss";
import cx from "classnames";
import {ButtonStyles} from "../Button/Button";
import Button from "../Button";
import {SCREENS} from "../../utils/constants";

const Header = ({onSecondScreen = false, logoClickHandler, changeScreen}) => {
    const [showSidePanel, setShowSidePanel] = useState(false);
    return (
        <div className={cx({[classes.header]: true, [classes.onSecondScreen]: onSecondScreen})}>
            <div className={classes.logo} onClick={() => logoClickHandler()}/>
            <div className={classes.border}/>
            <div className={classes.text}>
                 <div>Новый формат<br/>обслуживания автомобиля</div>
            </div>

            <div className={classes.hamburger} onClick={() => setShowSidePanel(true)}/>
            <div className={classes.sidePanel + ' ' + (showSidePanel ? classes.open : '')}>
                <div className={classes.shadow} onClick={() => setShowSidePanel(false)}/>
                <div className={classes.body}>
                    <div className={classes.close} onClick={() => setShowSidePanel(false)}/>
                    <div className={classes.logo}/>
                    <div className={classes.links}>
                        <div className={classes.link} onClick={() => {
                            setShowSidePanel(false);
                            changeScreen(SCREENS.gifts);
                        }}>Подарки</div>
                        <div className={classes.link} onClick={() => {
                            setShowSidePanel(false);
                            changeScreen(SCREENS.topServices);
                        }}>Услуги</div>
                        <div className={classes.link} onClick={() => {
                            setShowSidePanel(false);
                            changeScreen(SCREENS.oilCenter);
                        }}>Масляный бар</div>
                        <div className={classes.link} onClick={() => {
                            setShowSidePanel(false);
                            changeScreen(SCREENS.reviews);
                        }}>Отзывы</div>

                    </div>
                    <div  className={classes.spacer}/>
                    <div className={classes.contacts}>
                        <div className={classes.city}>г. Владивосток</div>
                        <div className={classes.address}>ул. Шилкинская, 32а</div>
                        <div className={classes.address}>ул. Снеговая, 1 стр. 3</div>
                    </div>
                    <div className={classes.icons}>
                        <a className={classes.phone} href="tel:+74232222999">+7 (423) 2222-999</a>
                        <a className={classes.whatsapp} href="https://wa.me/79140773596">+7 (423) 2222-999</a>
                    </div>
                    <Button handler={() => {
                        window.location.href = 'https://ortus.ru/w/2222999';
                    }} styles={[ButtonStyles.blue, ButtonStyles.fullWidth]}>Записаться</Button>
                </div>


            </div>
        </div>
    );
}

Header.propTypes = {
    onSecondScreen: PropTypes.bool,
    logoClickHandler: PropTypes.func
}

export default Header;