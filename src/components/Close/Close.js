import React from "react";
import PropTypes from 'prop-types';
import classes from "./close.module.scss";
import cx from 'classnames';

export const CloseStyles = {
    white: 'white',
    shadow: 'shadow',
    black: 'black'
};
const Close = ({styles, className, handler}) => {
    const styleClasses = {};
    (styles || []).forEach((style) => {
        styleClasses[classes[style]] = true;
    });
    return (
        <div className={classes.close + (className ? ' ' + className : '' ) + ' ' + cx(styleClasses)} onClick={() => {
           handler();
        }}/>
    );
}

Close.propTypes = {
    className: PropTypes.string,
    handler: PropTypes.func.isRequired,
    styles: PropTypes.arrayOf(PropTypes.oneOf(Object.values(CloseStyles))),
}

export default Close;