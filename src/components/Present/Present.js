import React from "react";
import PropTypes from 'prop-types';
import classes from "./present.module.scss";

const Present = ({id, title, description, image}) => {
    return (
        <div className={classes.present} data-id={id}>
            <div className={classes.image}>
                <img src={image} alt=''/>
            </div>
            <div className={classes.textPosition1}>
                <div className={classes.title}>{title.split('<br>').map(t => <>{t}<br/></>)}</div>
                <div className={classes.description}>{description.split('<br>').map(t => <>{t}<br/></>)}</div>
            </div>
        </div>
    );
}

Present.propTypes = {
    id: PropTypes.number,
    title: PropTypes.string,
    description: PropTypes.string,
    image: PropTypes.string
}

export default Present;