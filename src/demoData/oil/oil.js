export const marks = [
    {
        title: 'Toyota',
        logo: require('./logo--toyota.png'),
        id: 1,
    },
    {
        title: 'Lexus',
        logo: require('./logo--lexus.png'),
        id: 2
    },
    {
        title: 'Nissan',
        logo: require('./logo--nissan.png'),
        id: 3
    },
    {
        title: 'Subaru',
        logo: require('./logo--subaru.png'),
        id: 4
    },

];
export const oil = [
    {
        id: 1,
        title: 'Toyota',
        logo: require('./oil__logo--toyota.png'),
        price: [
            { property: 'Ow20', price: 1200 },
            { property: '5w30', price: 1150 }
        ]
    },
    {
        id: 6,
        title: 'Honda',
        logo: require('./oil__logo--honda.png'),
        price: [
            { property: 'Ow20', price: 1400 },
            { property: '5w30', price: 1200 }
        ]
    },
/*    {
        id: 1,
        title: 'Nissan',
        price: [
            { property: 'Ow20', price: 1550 },
            { property: '5w30', price: 1300 }
        ]
    },*/
    {
        id: 2,
        title: 'Idemitsu zero',
        logo: require('./oil__logo--idemitsu.png'),
        price: [
            { property: 'Ow20', price: 1450 },
            { property: '5w30', price: 1400 }
        ]
    },
    {
        id: 3,
        title: 'Castrol Magnatec',
        logo: require('./oil__logo--castrol.png'),
        price: [
            { property: 'Ow20', price: 1790 },
            { property: '5w30', price: 1650 }
        ]
    },
    {
        id: 4,
        title: 'Mobil 1',
        logo: require('./oil__logo--mobil1.png'),
        price: [
            { property: 'Ow20', price: 1950 },
            { property: '5w30', price: 1700 }
        ]
    },
    {
        id: 5,
        title: 'Shell Helix HX8',
        logo: require('./oil__logo--helix.png'),
        price: [
            { property: 'Ow20', price: 1200 },
            { property: '5w30', price: 1100 }
        ]
    }
];

export const cars = [
    {
        id: 1,
        markId: 1,
        mark: 'Toyota',
        model: 'Land Cruiser Prado',
        image: require('./toyota_land_cruiser_prado.jpg'),
        oilCapacity: 2
    },
    {
        id: 2,
        markId: 1,
        mark: 'Toyota',
        model: 'RAV-4',
        image: require('./toyota_rav4.jpg'),
        oilCapacity: 2
    },
    {
        id: 3,
        markId: 1,
        mark: 'Toyota',
        model: 'Harrier',
        image: require('./toyota_harrier.jpg'),
        oilCapacity: 3
    },
    {
        id: 4,
        markId: 2,
        mark: 'Lexus',
        model: 'LX570',
        image: require('./lexus_lx570.jpg'),
        oilCapacity: 2
    },
    {
        id: 5,
        markId: 2,
        mark: 'Lexus',
        model: 'GX460',
        image: require('./lexus_gx460.jpg'),
        oilCapacity: 3
    },
    {
        id: 6,
        markId: 2,
        mark: 'Lexus',
        model: 'RX400H',
        image: require('./lexus_rx400h.png'),
        oilCapacity: 4
    },
    {
        id: 7,
        markId: 3,
        mark: 'Nissan',
        model: 'X-Trail',
        image: require('./nissan_x-trail.png'),
        oilCapacity: 4
    },
    {
        id: 8,
        markId: 4,
        mark: 'Subaru',
        model: 'Forester',
        image: require('./subaru_forester.jpg'),
        oilCapacity: 5
    },
];