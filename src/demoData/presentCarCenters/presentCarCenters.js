export const presentCarCenters = [
    {
        id: 1,
        title: 'Toyota',
        label: 'toyota',
        image: require('./image--toyota.jpg')
    },
    {
        id: 2,
        title: 'Lexus',
        label: 'lexus',
        image: require('./image--lexus.jpg')
    },
    {
        id: 3,
        title: 'Nissan',
        label: 'nissan',
        image: require('./image--nissan.jpg')
    },
    {
        id: 4,
        title: 'Honda',
        label: 'honda',
        image: require('./image--honda.jpg')
    },
    {
        id: 5,
        title: 'Subaru',
        label: 'subaru',
        image: require('./image--subaru.jpg')
    },
    {
        id: 6,
        title: 'Mitsubishi',
        label: 'mitsubishi',
        image: require('./image--mitsubishi.jpg')
    },
    {
        id: 7,
        title: 'Hyundai',
        label: 'hyundai',
        image: require('./image--hyandai.jpg')
    }
];