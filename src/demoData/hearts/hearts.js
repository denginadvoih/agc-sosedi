export const hearts = [
    {
        id: 1,
        title: 'Есть повод стать ближе',
        icon: require('./icons/heart.png')
    },
    {
        id: 2,
        title: 'Вкусные цены',
        icon: require('./icons/rouble.png')
    },
    {
        id: 3,
        title: 'Зана ожидания<br>бизнес класса',
        icon: require('./icons/cup.png')
    },
    {
        id: 5,
        title: 'Максимум услуг рядом с домом',
        icon: require('./icons/car.png')
    },
    {
        id: 6,
        title: 'Профессиональные эксперты',
        icon: require('./icons/expert.png')
    },
    {
        id: 4,
        title: 'Профессиональные механики',
        icon: require('./icons/man.png')
    },
    {
        id: 7,
        title: 'Современные посты',
        icon: require('./icons/car2.png')
    },
    {
        id: 8,
        title: 'Современное оборудование',
        icon: require('./icons/wrench.png')
    },
    {
        id: 8,
        title: 'Расширенная гарантия',
        icon: require('./icons/shield.png')
    },

];