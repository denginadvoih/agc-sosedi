export const presentScreens = [
    {
        id: 2,
        title: '',
        description: 'Давайте знакомиться<br>Приглашаем на чашечку кофе',
        image: require('./images/image--2.jpg')
    },
    {
        id: 1,
        title: 'Мы рядом',
        description: 'Шилкинская 32а.<br>Доступность 10 мин.',
        image: require('./images/image--1.jpg')
    },
    {
        id: 5,
        title: '',
        description: 'Нам есть чем<br>вас удивить',
        image: require('./images/image--5.jpg')
    },
    {
        id: 6,
        title: '',
        description: 'Узнаем ваши<br>предпочтения',
        image: require('./images/image--6.jpg')
    },
    {
        id: 3,
        title: '',
        description: 'Всегда придём<br>на помощь',
        image: require('./images/image--3.jpg')
    },
    {
        id: 4,
        title: '',
        description: 'Ваш надёжный Автодруг<br>рядом с домом',
        image: require('./images/image--4.jpg')
    },
];