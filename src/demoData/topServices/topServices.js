export const topServices = [
    {
        id: 1,
        title: 'Профессиональная замена масла в двигателе',
        image: require('./service1.jpg')
    },
    {
        id: 3,
        title: 'Профессиональная диагностика ходовой части',
        image: require('./service3.jpg')
    },
    {
        id: 4,
        title: 'Профессиональный ремонт ходовой части',
        image: require('./service4.jpg')
    },
    {
        id: 6,
        title: 'Профессиональная установка GSM сигнализаций',
        image: require('./service6.jpg')
    },
    {
        id: 2,
        title: 'Профессиональная замена масла в автомате',
        image: require('./service2.jpg')
    },
    {
        id: 5,
        title: 'Профессиональная замена колодок',
        image: require('./service5.jpg')
    },
];