// noinspection SpellCheckingInspection
export const ROUTES = {
    main: '/sosedi/',
    readOn: '/readon',
    serviceGift: (serviceId) => '/services/' + serviceId + '/gift'
}