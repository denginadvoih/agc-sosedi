export const SCREENS = {
    main: 'main',
    contacts: 'contacts',
    acquaint: 'acquaint',
    hearts: 'hearts',
    topServices: 'topServices',
    oilCenter: 'oilCenter',
    reviews: 'reviews',
    gifts: 'gifts'
};
export const SHILKINSKAYA_LOCATION = [43.117962, 131.931152];
export const SNEGOVAIA_LOCATION = [43.136561, 131.927742];
export const FETCH_RESULT = { idle: 'idle', fail: 'fail', success: 'success' };
export const COMPANY_ID = 84;
export const RECAPTCHA_KEY = '6LcTwBMlAAAAAALnX1XhGlJsLMy1ApQuPIS25qkJ';
export const ORTUS_SCREEN_ID = 'ortusScreen';
