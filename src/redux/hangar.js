// ------------------------------------
// Constants
// ------------------------------------

const CHANGE_START_STATUS = 'changeStartStatus';
const SET_AJAX_ERROR = 'changeStartStatus';

// ------------------------------------
// Actions
// ------------------------------------

export const changeStartStatus = (status) => ({
    type: CHANGE_START_STATUS,
    payload: status
});


export const setAjaxError = (error) => {
    return {
        type: SET_AJAX_ERROR,
        payload: error
    };
}


// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
    [CHANGE_START_STATUS]: (state, action) => {
        return ({ ...state, startStatus: action.payload });
    }
};

let initialState = {
    startStatus: true
};

export default function hangarReducer(state = initialState, action) {

    state = Object.assign({}, initialState, state);

    const handler = ACTION_HANDLERS[action.type];

    return handler ? handler(state, action) : state;
}
