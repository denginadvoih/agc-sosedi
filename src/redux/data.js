// ------------------------------------
// Constants
// ------------------------------------
import get from 'lodash.get';
import {api, API_URL} from "../utils";
import {FETCH_RESULT} from "../utils/constants";

const FETCHING_ON = 'fetchingOn:data';
const FETCHING_OFF = 'fetchingOff:data';

const RECEIVE_REVIEWS = 'receiveReviews';
const RECEIVE_CATALOGUES = 'receiveCatalogues';
const RECEIVE_SERVICES = 'receiveServices';

const RECEIVE_SERVICE_CARDS = 'receiveServiceCards';

const MAIL_IS_SENT = 'data:mailIsSent';
const MAIL_SEND_RESET = 'data:mailSendReset';

// ------------------------------------
// Actions
// ------------------------------------
export const sendMail = (data) => {
    return function (dispatch) {
        dispatch({type: FETCHING_ON, payload: 'sendMail'});
        return api('https://www.autogarantcity.ru/mobile/ajax/index.php?method=sendMail',
            {
                method: 'POST',
                body: JSON.stringify(data)
            }, dispatch)
            .then((res) => {
                if (res) {
                    dispatch({type: MAIL_IS_SENT});
                }
                dispatch({type: FETCHING_OFF, payload: 'sendMail'});
            })
    }
}
export const fetchServices = () => {
    return function (dispatch) {
        dispatch({type: FETCHING_ON, payload: 'fetchServices'});
        return api('https://api.ortus.ru/v1/widgets/13/services?expand=serviceOptions,serviceTemplate,serviceOptionTemplates,serviceOption.service,serviceOptionReference,reference,values', {method: 'GET'}, dispatch)
            .then((res) => {
                if (res) {
                    dispatch({type: RECEIVE_SERVICES, payload: res});
                }
                dispatch({type: FETCHING_OFF, payload: 'fetchServices'});
            })
    }
}
export const fetchCatalogues = () => {
    return function (dispatch) {
        dispatch({type: FETCHING_ON, payload: 'fetchCatalogues'});
        return api('https://api.ortus.ru/v1/widgets/13/catalogues', {method: 'GET'}, dispatch)
            .then((res) => {
                if (res) {
                    dispatch({type: RECEIVE_CATALOGUES, payload: res});
                }
                dispatch({type: FETCHING_OFF, payload: 'fetchCatalogues'});
            })
    }
}
export const fetchReviews = () => {
    return function (dispatch) {
        dispatch({type: FETCHING_ON, payload: 'fetchReviews'});
        return api('https://www.autogarantcity.ru/mobile/ajax/index.php?method=reviews', {method: 'GET'}, dispatch)
            .then((res) => {
                if (res) {
                    dispatch({type: RECEIVE_REVIEWS, payload: res});
                }
                dispatch({type: FETCHING_OFF, payload: 'fetchReviews'});
            })
    }
}
export const fetchServiceCards = () => {
    return function (dispatch) {
        dispatch({type: FETCHING_ON, payload: 'fetchServiceCards'});
        return api('https://www.autogarantcity.ru/mobile/ajax/index.php?method=service_cards', {method: 'GET'}, dispatch)
            .then((res) => {
                if (res) {
                    dispatch({type: RECEIVE_SERVICE_CARDS, payload: res});
                }
                dispatch({type: FETCHING_OFF, payload: 'fetchServiceCards'});
            })
    }
}

function flatCatalogues(catalogues) {
    const result = [];
    const springItems = (result, brunch) => {
        brunch.forEach(c => {
            result.push({
                id: c.id,
                name: c.name,
                parentCatalogueId: get(c, 'parentCatalogue.id', null),
                serviceTemplates: c.serviceTemplates.map(st => ({
                    ...st,
                    services: st.services.filter(s => s.isHidden !== true)
                })),
                sort: c.sort
            });
            springItems(result, c.childrenCatalogues || []);
        });

    }
    springItems(result, catalogues);
    return result;
}

export const sendMailReset = () => {
    return {
        type: MAIL_SEND_RESET
    }
}


// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
    [FETCHING_ON]: (state, action) => {
        const fetching = Object.assign({}, state.fetching);
        fetching[action.payload] = true;
        return ({...state, fetching});
    },
    [FETCHING_OFF]: (state, action) => {
        const fetching = Object.assign({}, state.fetching);
        fetching[action.payload] = false;
        return ({...state, fetching});
    },
    [RECEIVE_REVIEWS]: (state, action) => {
        return ({
            ...state,
            reviews: action.payload.map(r => ({
                id: parseInt(r.id),
                origin: r.section.name,
                image: API_URL + r.detail_picture
            }))
        });
    },
    [RECEIVE_CATALOGUES]: (state, action) => {
        const catalogues = flatCatalogues(action.payload.items);
        return ({...state, catalogues});
    },
    [RECEIVE_SERVICES]: (state, action) => {
        return ({...state, services: action.payload});
    },
    [RECEIVE_SERVICE_CARDS]: (state, action) => {
        return ({...state, serviceCards: action.payload});
    },
    [MAIL_IS_SENT]: (state) => {
        return ({...state, mailIsSent: FETCH_RESULT.success});
    },
    [MAIL_SEND_RESET]: (state) => {
        return ({...state, mailIsSent: FETCH_RESULT.idle});
    }
};

let initialState = {
    catalogues: [],
    services: {},
    serviceCards: null,
    fetching: {},
    reviews: null,
    mailIsSent: FETCH_RESULT.idle
};

export default function dataReducer(state = initialState, action) {

    state = Object.assign({}, initialState, state);

    const handler = ACTION_HANDLERS[action.type];

    return handler ? handler(state, action) : state;
}
