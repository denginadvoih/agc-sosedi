import { combineReducers } from "redux";
import hangar from "./hangar.js";
import data from "./data.js";

export default combineReducers({ hangar, data });