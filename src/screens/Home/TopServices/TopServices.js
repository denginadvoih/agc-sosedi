import React from 'react';
import PropTypes from 'prop-types';
import { Swiper, SwiperSlide } from 'swiper/react';
// Import Swiper styles
import 'swiper/css';
import classes from './topServices.module.scss'

import {SCREENS} from "utils/constants";
import TopService from 'components/TopService';

const TopServices = ({topServices}) => {
    return <>
        <div className={classes.screen} id="topServicesId">
            <div id={SCREENS.topServices}/>
            <div className={classes.greetingScreen}>
                <div>Максимум услуг<br/><span>в одном месте</span></div>
                <div>Расширенная гарантия<br/>на все виды работ</div>
            </div>
            <div className={classes.present} id="topServicesSwiper">
                <Swiper
                    slidesPerView={2}
                    slidesOffsetBefore={document.documentElement.clientWidth / 100 * 8}

                >
                    {
                        topServices.map((service, key) => <SwiperSlide key={key}><TopService {...service} /></SwiperSlide>)
                    }
                    <SwiperSlide key={topServices.length}><TopService final/></SwiperSlide>
                    <SwiperSlide key={topServices.length}><div/></SwiperSlide>
                </Swiper>

            </div>
        </div>
    </>;
}

TopServices.propTypes = {

}


export default TopServices;