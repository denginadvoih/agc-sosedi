import {connect} from "react-redux";
import { fetchServiceCards } from "redux/data";
import Services from './Services.js';

const mapStateToProps = state => {
    return {
        serviceCards: state.data.serviceCards
    }
};

export default connect(mapStateToProps, { fetchServiceCards })(Services);