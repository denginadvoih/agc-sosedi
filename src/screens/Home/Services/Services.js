import React, {useEffect} from "react";
import PropTypes from 'prop-types';
import 'swiper/css';
import classes from './services.module.scss'
import PresentService from "./PresentService";
import Catalogues from "../Catalogues";

const Services = ({ serviceCards, fetchServiceCards }) => {

    useEffect(() => {
        fetchServiceCards();
    }, []);

    return <div className={classes.screen}>
        <div className={classes.header}>
            <div>Давайте знакомиться!</div>
            <div>Запишитесь на нужную услугу и почувствуйте разницу</div>
        </div>
        <div className={classes.services}>
            {(serviceCards || []).map(s => <PresentService key={s.id} {...s} />)}
        </div>
        <Catalogues/>
    </div>;
}

Services.propTypes = {
    services: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        description: PropTypes.string,
        angaraId: PropTypes.number,
    })),
    presentServices: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        description: PropTypes.string,
        angaraId: PropTypes.number,
        image: PropTypes.string,
    })),

}

export default Services;