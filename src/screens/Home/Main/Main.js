import React from "react";
import classes from './main.module.scss'
import Button from "components/Button";
import {SCREENS} from "utils/constants";

const Main = ({changeScreen}) => {
    return <div className={classes.screen}>
        <div className={classes.greeting}>Добро пожаловать</div>
        <div className={classes.introduce}>Меня зовут Любовь</div>
        <div className={classes.introduce}>Что вас интересует?</div>
        <div className={classes.buttons}>
            <Button handler={() => changeScreen(SCREENS.contacts)}>Контакты</Button>
            <Button handler={() => {
                window.location.href = 'https://ortus.ru/w/2222999';
            }}>Записаться</Button>
            <Button handler={() => changeScreen(SCREENS.acquaint)} large>Познакомиться с компанией</Button>
            <Button handler={() => changeScreen(SCREENS.contacts)} large>Порекомендовать другу</Button>
        </div>
    </div>;
}
export default Main;