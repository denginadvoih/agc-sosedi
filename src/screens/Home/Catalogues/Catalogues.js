import React, {useEffect, useState} from "react";
import PropTypes from 'prop-types';
import cloneDeep from 'lodash.clonedeep';
import 'swiper/css';
import classes from './catalogues.module.scss'
import cx from "classnames";

const Catalogues = ({rawCatalogues, rawServices, fetchCatalogues, fetchServices}) => {
    const [openCatalogues, setOpenCatalogues] = useState([]);
    const [catalogues, setCatalogues] = useState([]);
    const [services, setServices] = useState({});
    useEffect(() => {
        fetchCatalogues();
    }, [fetchCatalogues]);
    useEffect(() => {
        fetchServices();
    }, [fetchServices]);
    useEffect(() => {
        setCatalogues(autoGarantCityHardCoreCat(rawCatalogues))
    }, [rawCatalogues]);
    useEffect(() => {
        setServices(autoGarantCityHardCoreSer(rawServices))
    }, [rawServices]);

    const autoGarantCityHardCoreCat = (_catalogues) => {
        const result = cloneDeep(_catalogues);
        const autoRepair = result.find(c => c.alias = 'avtoservice');
        if (autoRepair) {
            autoRepair.name = 'АвтоРемонт';
        }
        return result;
    }
    const autoGarantCityHardCoreSer = (_services) => {
        return cloneDeep(_services);
    }

    const toggleCatalogues = (catalogueId) => {
        const _openCatalogues = Array.from(openCatalogues);
        if (openCatalogues.indexOf(catalogueId) === -1) {
            _openCatalogues.push(catalogueId);
        } else {
            _openCatalogues.splice(openCatalogues.indexOf(catalogueId));
        }
        setOpenCatalogues(_openCatalogues);
    }

    const renderBrunch = (parentCatalogueId) => {
        const cataloguesBy = catalogues.filter(c => c.parentCatalogueId === parentCatalogueId)
            .sort(function compareNumbers(a, b) {
                return a.sort - b.sort;
            });
        const catalogueServices = ((services.items || []).filter(s => s.serviceTemplate.catalogue.id === parentCatalogueId) || {}).sort(function compareNumbers(a, b) {
            return a.serviceTemplate.sort - b.serviceTemplate.sort;
        });

        return (
            <div className={cx({
                [classes.brunch]: true,
                [classes.hidden]: openCatalogues.indexOf(parentCatalogueId) === -1,
                [classes.withSelected]: cataloguesBy.some(c => openCatalogues.indexOf(c.id) !== -1)
            })}>
                {parentCatalogueId === null
                    ?
                    (
                        <div className={classes.triangle}>

                        </div>
                    )
                    : null
                }

                {cataloguesBy.map(c => {
                    return (
                        <div
                            key={c.id}
                            className={cx({
                            [classes.catalogue]: true,
                            [classes.selected]: openCatalogues.indexOf(c.id) !== -1,
                            [classes.withBrunch]: c.serviceTemplates.length > 0 || catalogues.some(child => child.parentCatalogueId === c.id),

                        })}>
                            <div className={classes.title}  data-alias={c.alias} onClick={(e) => {
                                e.stopPropagation();
                                toggleCatalogues(c.id);
                            }
                            }>{c.name}</div>
                            {renderBrunch(c.id)}
                        </div>)
                })}
                {catalogueServices.map(s => {
                    return (
                        <div key={s.id} className={classes.serviceTemplate} onClick={() => {
                            window.location.href = 'https://ortus.ru/w/2222999?service_id=' + s.id;
                        }}>
                            <div className={classes.title}>{s.serviceTemplate.name}</div>
                        </div>
                    )
                })}
                    </div>
                    );
                }
    return <>
        <div className={cx({[classes.selector]: true, [classes.selected]: openCatalogues.length > 0})} onClick={() => toggleCatalogues(null)}>
            <div>Каталог услуг</div>
            <div className={classes.select}>
                Выбрать услугу
            </div>
        </div>
            {renderBrunch(null)}
    </>;
}

Catalogues.propTypes = {
    catalogues: PropTypes.array
}

export default Catalogues;