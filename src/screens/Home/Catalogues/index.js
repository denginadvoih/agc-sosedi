import {connect} from "react-redux";
import { fetchCatalogues, fetchServices } from "redux/data";
import Catalogues from './Catalogues.js';

const mapStateToProps = state => {
    return {
        rawCatalogues: state.data.catalogues,
        rawServices: state.data.services
    }
};

export default connect(mapStateToProps, { fetchCatalogues, fetchServices })(Catalogues);