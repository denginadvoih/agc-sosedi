import React, {useState} from "react";
import PropTypes from 'prop-types';
import classes from './carCenters.module.scss'
import CarCenter from "./CarCenter";
import CarSplitter from "./CarSplitter";

const CarCenters = ({ carCenters }) => {
    const [toyotaNotLexus, setToyotaNotLexus] = useState(true);
    const toyota = carCenters.find(c => c.label === 'toyota');
    const lexus = carCenters.find(c => c.label === 'lexus');
    return <div className={classes.screen}>
        <div className={classes.divisions}>
            <CarSplitter
            car1={toyota}
            car2={lexus}
            showFirstCar={toyotaNotLexus}
            />
            {carCenters.filter(c => ['toyota', 'lexus'].indexOf(c.label) === -1)
                .map(c => <CarCenter key={c.id} {...c} />)}
        </div>
    </div>;
}

CarCenters.propTypes = {
    carCenters: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string,
        image: PropTypes.string,
    })),

}

export default CarCenters;