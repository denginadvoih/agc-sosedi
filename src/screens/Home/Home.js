import React, {useEffect, useState} from "react";
import Header from "components/Header";
import Contacts from "./Contacts";
import Acquaint from "./Acquaint";
import classes from './styles.module.scss'
import {SCREENS} from "utils/constants";
import Footer2 from "components/Footer2";
import {presentScreens} from "demoData/presentScreens/presentScreens";
import {hearts} from "demoData/hearts/hearts";
import Hearts from './Hearts';
import Reviews from './Acquaint/Reviews';
import { topServices } from 'demoData/topServices/topServices';
import { cars, oil, marks } from 'demoData/oil/oil';
import Ortus from './Ortus';
import GiftSections from './Gifts';
import TopServices from './TopServices';
import OilCenter from './OilCenter';
import Compare from '../../components/Compare';
import FastWith from '../../components/FastWith';


function Home({startStatus}) {
    const [isScrolled, setIsScrolled] = useState(false);
    const [showArrowToTop, setShowArrowToTop] = useState(false);
    const [showFooter, setShowFooter] = useState(false);
    const [screen, setScreen] = useState(SCREENS.main);

    const preventDefault = (e) => {
        if (startStatus) {
            e.preventDefault();
        }
    };

    useEffect(() => {
        document.body.addEventListener('touchmove', preventDefault, { passive: false });
        return document.body.removeEventListener('touchmove', preventDefault);
    }, []);

    useEffect(() => {
        const handleScroll = e => {
            const scrollValue = e.target.documentElement.scrollTop;
            setIsScrolled(scrollValue > 20);
            setShowFooter(scrollValue > 0.8 * e.target.documentElement.clientHeight);
            setShowArrowToTop(scrollValue > 1.4 * e.target.documentElement.clientHeight);
        };
        window.addEventListener('scroll', handleScroll);
        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    useEffect(() => {
        const element = document.getElementById(screen);
        if (element) {
            element.scrollIntoView({behavior: 'smooth'});
            setScreen(null);
        }
    }, [screen]);

    const scrollToTop = () => {
        const element = document.getElementById(SCREENS.main);
        if (element) {
            element.scrollIntoView({behavior: 'smooth'});
        }
    }

    return (
        <div className={classes.scrollingArea}>
            <Header onSecondScreen={isScrolled} logoClickHandler={() => {
                if (screen !== SCREENS.main) {
                    setScreen(SCREENS.main);
                } else {
                    scrollToTop();
                }
            }}
                    changeScreen={(screen) => {
                        setScreen(screen);
                    }}
            />
            <div id={SCREENS.main}/>
            <Acquaint presents={presentScreens}/>
            <div id={SCREENS.hearts}/>
            <Hearts hearts={hearts}/>
            <GiftSections/>
            <TopServices topServices={topServices}/>
            <Compare/>
            <OilCenter oil={oil} cars={cars} marks={marks}/>
            <Ortus />
            <Reviews/>
            <FastWith/>
            <div id={SCREENS.contacts}/>
            <Contacts/>
            <div className={classes.arrow + ' ' + (showArrowToTop ? classes.visible : '')} onClick={() => scrollToTop()}/>
            <Footer2 active={showFooter}/>
        </div>
    );
}

export default Home;
