import {connect} from "react-redux";
import { changeStartStatus } from "redux/hangar";
import Home from './Home.js';

const mapStateToProps = state => {
    return {
        startStatus: state.hangar.startStatus
    }
};

export default connect(mapStateToProps, { changeStartStatus })(Home);