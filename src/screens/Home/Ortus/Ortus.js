import React from "react";
import classes from './ortus.module.scss'
import { ORTUS_SCREEN_ID } from 'utils/constants';
import { Swiper, SwiperSlide, useSwiper } from 'swiper/react';
import Button from '../../../components/Button';
import { ButtonStyles } from '../../../components/Button/Button';
import { Pagination } from 'swiper';

const Ortus = () => {
    return (
        <div className={classes.screen} id={ORTUS_SCREEN_ID}>
            <Swiper
                slidesPerView={1}
                loop
                modules={[Pagination]}
                pagination={{clickable: true, el: "#paginationElementForOrtus", horizontalClass: classes.pagination, bulletClass: classes.bullet, bulletActiveClass: classes.active}}
            >
                <SwiperSlide key="fivePercent">
                    <div className={classes.screen + ' ' + classes.fivePercent}>
                        <div className={classes.text1}>Скидка 5%</div>
                        <div className={classes.text2}>при онлайн записи через</div>
                        <div className={classes.text3}>электронную сервисную книжку</div>
                        <div className={classes.discount}/>
                        <div className={classes.text4 + ' ' + classes.fly}>Залетай</div>
                    </div>
                </SwiperSlide>
                <SwiperSlide key="ortus">
                    <div className={classes.screen + ' ' + classes.ortus}>
                        <div className={classes.header}>
                             <div className={classes.greetings2}>
                                <div className={classes.logo}/>
                                <div className={classes.title}>Ortus</div>
                                <div className={classes.border}/>
                                <div className={classes.about}>Электронная<br/>сервисная книжка</div>
                            </div>
                        </div>
                        <div className={classes.ortusMaket}/>
                        <div className={classes.download}>Скачай</div>
                        <div className={classes.buttons}>
                            <a href="https://play.google.com/store/apps/details?id=ru.ortus.assistant" className={classes.toGoogle}/>
                            <a href="https://itunes.apple.com/ru/app/id1189488678" className={classes.toIos}/>
                        </div>
                    </div>
                </SwiperSlide>
                <div className={classes.buttonPosition}>
                    <ButtonSlidePrev /><ButtonSlideNext />
                </div>
                <div id="paginationElementForOrtus"/>
            </Swiper>




        </div>
    );
}
const ButtonSlideNext = () => {
    const swiper = useSwiper();
    return <Button styles={[ButtonStyles.circle, ButtonStyles.arrowRight, ButtonStyles.whiteBorder]} handler={() => {
        swiper.slideNext();
    }}/>
}

const ButtonSlidePrev = () => {
    const swiper = useSwiper();
    return <Button styles={[ButtonStyles.circle, ButtonStyles.arrowLeft, ButtonStyles.whiteBorder]} handler={() => {
        swiper.slidePrev();
    }}/>
}
export default Ortus;