import React from "react";
import PropTypes from 'prop-types';
import 'swiper/css';
import classes from './divisions.module.scss'
import Division from "./Division";
import {ButtonStyles} from "components/Button/Button";
import Button from "components/Button";
import {ROUTES} from "../../../routes";
import {useNavigate} from "react-router-dom";

const Divisions = ({ divisions }) => {
    // const navigate = useNavigate();
    return <>
        <div className={classes.screen}>
        <div className={classes.header}>
            <div className={classes.logo}/>
            <div className={classes.text}>Сеть специализированных<br/>автосервисов</div>
        </div>
        <div className={classes.divisions}>
            {divisions.map(g => <Division key={g.id} {...g} />)}
        </div>

    </div>
        <div className={classes.ladyClub}>
            <div className={classes.image}>
                <img src={require('./assets/ladyClub.jpg')} alt='' />
            </div>
            <div className={classes.title}>Специализированный автоцентр</div>
            <div className={classes.title2}>АвтоЛеди</div>
            <div className={classes.title3}>Профессиональный ремонт</div>
            {/*<Button styles={[ButtonStyles.blue, ButtonStyles.small]} handler={() => navigate(ROUTES.readOn)}>Подробно</Button>*/}
        </div>
    </>;
}

Divisions.propTypes = {
    giftSections: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        description: PropTypes.string,
        image: PropTypes.string,
    })),

}

export default Divisions;