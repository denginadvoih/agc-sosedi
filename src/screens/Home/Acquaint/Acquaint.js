import React, { useEffect, useLayoutEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import {Swiper, SwiperSlide, useSwiper} from 'swiper/react';
import {Pagination} from 'swiper';
// Import Swiper styles
import 'swiper/css';
import 'swiper/scss/pagination';
import classes from './acquaint.module.scss'
import Present from "components/Present";
import Button from "components/Button";
import {ButtonStyles} from "components/Button/Button";
import {SCREENS} from "../../../utils/constants";

const Acquaint = ({presents}) => {
    const [offsetPosition, setOffsetPosition] = useState(0);
    const [currentPresentId, setCurrentPresentId] = useState(0);

    const getButtonsPosition = () => {
        try {
            const parentPos = document.getElementById('parentId').getBoundingClientRect();
            const childPos  = document.getElementById('childId').getBoundingClientRect();
            const buttonPos  = document.getElementById('readonButton').getBoundingClientRect();
            const footerPos = document.getElementById('footer').getBoundingClientRect();
            return childPos.top + (childPos.height - ((childPos.height - buttonPos.height) / 2)) + footerPos.height - parentPos.top;
        } catch (e) {
            return 0;
        }
    }

    useLayoutEffect(() => {
        const windowInnerHeight = window.innerHeight;
        const buttonPos = getButtonsPosition();
        const offset = windowInnerHeight - buttonPos;
        setOffsetPosition(offset);
    }, []);

    return <>
        <div className={classes.screen} id="parentId">
            <div id={SCREENS.acquaint} style={{position: 'absolute', top: - offsetPosition + 'px'}}/>
            <div className={classes.greetingScreen}>
                <div>Ваш близкий сосед.</div>
                <div>Современный автосервис.</div>
                <div>Экономьте время с профессионалами. Обслуживайте автомобиль с комфортом.</div>
            </div>
            <div className={classes.present} id="presentSwiper">
                <Swiper

                    slidesPerView={1}
                    onSlideChange={(swiper) => setCurrentPresentId(presents[swiper.realIndex].id)}
                    onSwiper={(swiper) => console.log(swiper)}
                >
                    {
                        presents.map((present, key) => <SwiperSlide key={key}><Present {...present} /></SwiperSlide>)
                    }
                    <div className={classes.buttonPosition} id="childId" data-slidenumber={currentPresentId}>
                        <SwiperController/>
                    </div>
                </Swiper>

            </div>
        </div>
    </>;
}

const SwiperController = () => {
    const swiper = useSwiper();

    useEffect(() => {
        const handleScroll = e => {
            const scrollValue = e.target.documentElement.scrollTop;
            const swiperPos  = document.getElementById('presentSwiper').getBoundingClientRect();
            if (swiperPos) {
                const initialTop = swiperPos.top + scrollValue;
                if (swiperPos.top >= - swiperPos.height && swiperPos.top <= initialTop) {
                    const progress = 100 - Math.round((swiperPos.top + swiperPos.height) / ((initialTop + swiperPos.height) / 100));
                    swiper.setProgress((progress / 100 / 2), 100);
                }
            }
        };
        window.addEventListener('scroll', handleScroll);
        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    return <></>;
}

const ButtonSlideNext = () => {
    const swiper = useSwiper();
    return <Button styles={[ButtonStyles.circle, ButtonStyles.arrowRight, ButtonStyles.gray]} handler={() => {
        swiper.slideNext();
    }}/>
}

const ButtonSlidePrev = () => {
    const swiper = useSwiper();
    return <Button styles={[ButtonStyles.circle, ButtonStyles.arrowLeft, ButtonStyles.gray]} handler={() => {
        swiper.slidePrev();
    }}/>
}

Acquaint.propTypes = {
    presents: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        description: PropTypes.string
    }))
}


export default Acquaint;