import React, {useEffect, useState} from "react";
import PropTypes from 'prop-types';
import {Swiper, SwiperSlide, useSwiper} from 'swiper/react';
import {Pagination} from 'swiper';
// Import Swiper styles
import 'swiper/css';
import classes from './reviews.module.scss'
import Header from "./Header";
import Review from "./Review";
import {ButtonStyles} from "components/Button/Button";
import Button from "components/Button";
import Rating, {RatingsOrigin} from "./Rating/Rating";
import './pagination.scss';
import { SCREENS } from '../../../../utils/constants';

const Reviews = ({reviews, fetchReviews}) => {
    const [startScreen, setStartScreen] = useState(true);
    useEffect(() => {
        fetchReviews();
    }, [fetchReviews]);

    const scrollOnSwipe = () => {
        const element = document.getElementById('reviewsPosition');
        if (element) {
            element.scrollIntoView({behavior: 'smooth'});
        }
    };
    const scrollOnEndSwipe = () => {
        const element = document.getElementById('reviewsPositionOnStart');
        if (element) {
            element.scrollIntoView({behavior: 'smooth'});
        }
    };

    return <div className={classes.screen}>
        <div className={classes.reviews}>
            <div id={SCREENS.reviews} className={classes.reviewsPosition}/>
            <Header/>
            <div id="reviewsPosition" className={classes.reviewsPosition}/>
            <div className={classes.swipeWrapper + ' ' + (startScreen ? classes.startScreen : '')}>
                <Swiper
                    spaceBetween={0}
                    slidesPerView={1}
                    onSlideChangeTransitionEnd={(swiper) => {
                        setStartScreen(swiper.realIndex === 0);
                        if (swiper.previousIndex !== 0 && swiper.realIndex === 0) {
                            scrollOnEndSwipe();
                        } else {
                            if (swiper.previousIndex !== 0) {
                                scrollOnSwipe();
                            }
                        }
                    }}

                    loop
                    autoHeight={true}
                    modules={[Pagination]}
                    pagination={{clickable: true}}
                >
                    <SwiperSlide>
                        <div className={classes.ratings}>
                            <div className={classes.subHeader}>
                                <div>
                                    Рейтинги
                                </div>
                                <ButtonForFirstSlide/>
                            </div>
                            <div className={classes.ratingsList}>
                                <Rating origin={RatingsOrigin.ortus} value={4.9}/>
                                <Rating origin={RatingsOrigin.vl} value={4.8}/>
                                <Rating origin={RatingsOrigin.yandex} value={4.7}/>
                                <Rating origin={RatingsOrigin.google} value={4.6}/>
                                <Rating origin={RatingsOrigin.twogis} value={3.3}/>
                            </div>
                        </div>
                    </SwiperSlide>
                    {
                        (reviews || []).map((review, key) => <SwiperSlide
                            key={key}><Review {...review} /></SwiperSlide>)
                    }
                </Swiper>
            </div>
        </div>
    </div>;
}

const ButtonForFirstSlide = () => {
    const swiper = useSwiper();
    return <Button styles={[ButtonStyles.arrowRight, ButtonStyles.circle, ButtonStyles.gray]} handler={() => {
        swiper.slideNext();
    }}/>
}

Reviews.propTypes = {
    reviews: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        origin: PropTypes.string,
        image: PropTypes.string,
    }))
}

export default Reviews;