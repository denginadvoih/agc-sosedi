import {connect} from "react-redux";
import { fetchReviews } from "redux/data";
import Reviews from './Reviews.js';

const mapStateToProps = state => {
    return {
        reviews: state.data.reviews
    }
};

export default connect(mapStateToProps, { fetchReviews })(Reviews);