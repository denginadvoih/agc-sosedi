import React from 'react';
import classes from './header.module.scss'

const Header = () => {
    return (
        <div className={classes.header}>
            <div className={classes.wrapper}>
                <div>Честные отзывы</div>
                <div>Наших гостей</div>
            </div>
        </div>
    );
}

export default Header;