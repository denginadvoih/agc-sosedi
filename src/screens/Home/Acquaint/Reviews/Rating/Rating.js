import React from "react";
import classes from './rating.module.scss'

export const RatingsOrigin = {
    ortus: 'ortus',
    vl: 'vl',
    yandex: 'yandex',
    google: 'google',
    twogis: 'twogis'
};

const Rating = ({origin, value}) => {

    return <div className={classes.rating + ' ' + classes[origin]}>
        <div className={classes.stars}>
            <div className={classes.value} style={{width: (value / (5 / 100)) + '%'}}></div>
        </div>
        <div className={classes.value}>{value}</div>
    </div>;
}

export default Rating;