import React from 'react';
import classes from './mark.module.scss';

const Mark = ({ title, logo, handler, active }) => {
    return (<div className={classes.mark + (active ? ' ' + classes.active : '')} onClick={() => handler()}>
        { logo ? (<div className={classes.logo} style={{ backgroundImage: "url(" + logo + ")" }} />) : null }
        <div className={classes.title}>{title}</div>
    </div>);
};
export default Mark;