import React, { useState } from 'react';
import { Swiper, SwiperSlide, useSwiper } from 'swiper/react';
// Import Swiper styles
import 'swiper/css';
import classes from './oilCenter.module.scss'

import { SCREENS } from 'utils/constants';
import Mark from './Mark';

import PropTypes from 'prop-types';
import Button from '../../../components/Button';
import { ButtonStyles } from '../../../components/Button/Button';

const OilCenter = ({ oil, cars, marks }) => {

    const [selectedMark, setSelectedMark] = useState(marks[0]);
    const [selectedOil, setSelectedOil] = useState([]);
    const selectOil = (o) => {
        setSelectedOil((selectedOil) => {
            const _selectedOil = Array.from(selectedOil);
            if (_selectedOil.indexOf(o.id) !== -1) {
                _selectedOil.splice(selectedOil.indexOf(o.id));
            } else {
                _selectedOil.push(o.id);
            }
            return _selectedOil;
        });

    }

    return <>
        <div className={classes.screen} id="oilCenterId">
            <div id={SCREENS.oilCenter} className={classes.oilCenterPosition}/>
            <div className={classes.greetingScreen}>
                <div>Замена масла в двигателе</div>
                <div>Масляный бар</div>
            </div>
            <div className={classes.swiperWrapper} id="topServicesSwiper">
                <Swiper
                    slidesPerView={4}
                    loop
                >
                    {
                        marks.map((mark, key) => <SwiperSlide key={key}><Mark {...mark}
                                                                              handler={() => setSelectedMark(mark)}
                                                                              active={mark.id === selectedMark?.id}/></SwiperSlide>)
                    }
                </Swiper>
            </div>
            <div className={classes.swiperWrapperForCars}>
                <Swiper
                    slidesPerView={1}
                    loop
                >
                    {cars.filter(car => car.markId === selectedMark.id).map(car => {
                        return (
                            <SwiperSlide key={car.id}>
                                <div className={classes.carDetailed}>
                                    <div className={classes.image}>
                                        <img src={car.image} alt=""/>
                                    </div>
                                    <div className={classes.title}>
                                        {selectedMark.title} {car.model}
                                    </div>
                                    <div className={classes.oilTable}>
                                        {oil.map(o => {
                                            return <div
                                                className={classes.row + (selectedOil.indexOf(o.id) !== -1 ? ' ' + classes.active : '')}>
                                                <div className={classes.logo}><img src={o.logo} alt={''} /></div>
                                                <div className={classes.title}>{o.title}</div>
                                                <div
                                                    className={classes.openIcon + (selectedOil.indexOf(o.id) !== -1 ? ' ' + classes.active : '')}
                                                    onClick={() => selectOil(o)}
                                                />
                                                <div className={classes.priceCapacity}>
                                                    {o.price.map(oPrice => (
                                                        <div>{oPrice.title} -
                                                            <span>{oPrice.price * car.oilCapacity} ({oPrice.price} Р. за литр)</span></div>
                                                    ))}
                                                </div>
                                            </div>
                                        })}
                                    </div>
                                </div>
                            </SwiperSlide>)
                    })}
                    <div className={classes.buttonPosition}>
                        <ButtonSlidePrev /><ButtonSlideNext />
                    </div>
                </Swiper>
            </div>
            <div className={classes.notFound}>
                <div className={classes.title}>
                    Не нашли свой автомобиль?
                </div>
                <div>
                    <Button styles={[ButtonStyles.darkBlue, ButtonStyles.fullWidth]} handler={() => {
                        window.location.href = 'tel:+74232222999';
                    }}>ПОМОЖЕМ С ВЫБОРОМ</Button>
                </div>
                <div>
                    <Button styles={[ButtonStyles.whiteBorder, ButtonStyles.fullWidth]} handler={() => {
                        window.location.href = 'tel:+74232222999';
                    }}>ПРАЙС-ЛИСТ</Button>
                </div>
            </div>
            <div className={classes.oilPresentations}>
                <div className={classes.wrapper}>
                    <div className={classes.sticker}>
                        <div className={classes.text}>
                            <div>гарантия от подделок</div>
                            <div>масляный бар</div>
                            <div>100%</div>
                        </div>

                    </div>
                    <div className={classes.block}>
                        <div className={classes.title}>
                            Ассортимент <span>настоящих масел</span>
                        </div>
                        <div className={classes.oilIcons}/>
                        <Button styles={[ButtonStyles.blue, ButtonStyles.fullWidth]} handler={() => {
                            window.location.href = 'tel:+74232222999';
                        }}>Экспертная консультация</Button>
                    </div>
                </div>

            </div>


        </div>
    </>
;
}

OilCenter.propTypes =
    {
        cars: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number,
            mark: PropTypes.string,
            model: PropTypes.string,
            image: PropTypes.string,
            oilCapacity: PropTypes.number
        })),
            oil:
        PropTypes.arrayOf(PropTypes.shape(
            {
                id: PropTypes.number,
                title: PropTypes.string,
                price: PropTypes.arrayOf(PropTypes.shape(
                    { property: PropTypes.string, price: PropTypes.number }
                ))
            }
        ))
    }


export default OilCenter;

const ButtonSlideNext = () => {
    const swiper = useSwiper();
    return <Button styles={[ButtonStyles.circle, ButtonStyles.arrowRight, ButtonStyles.gray]} handler={() => {
        swiper.slideNext();
    }}/>
}

const ButtonSlidePrev = () => {
    const swiper = useSwiper();
    return <Button styles={[ButtonStyles.circle, ButtonStyles.arrowLeft, ButtonStyles.gray]} handler={() => {
        swiper.slidePrev();
    }}/>
}