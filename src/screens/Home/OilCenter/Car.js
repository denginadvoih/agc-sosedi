import React from 'react';
import classes from './car.module.scss';

const Car = ({ mark, model, handler, active }) => {
    return (<div className={classes.car + (active ? ' ' + classes.active : '')} onClick={() => handler()}>
        <div className={classes.title}>
            {model === 'Land Cruiser Prado' ? (<>Toyota Land<br/> Cruiser Prado</>)
                :
                (<>{mark}<br/>{model}</>)
            }
        </div>
    </div>);
}
export default Car;