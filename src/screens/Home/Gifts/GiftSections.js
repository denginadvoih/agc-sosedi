import React, { useState } from 'react';
import classes from './giftSections.module.scss'
import Button from 'components/Button';
import { ButtonStyles } from 'components/Button/Button';
import cx from 'classnames';
import { SCREENS } from '../../../utils/constants';
import GiftForDating from '../../../components/GiftForDating/GiftForDating';
import { giftForDating } from '../../../demoData/giftForDating/giftForDating';

const GiftSections = () => {
    const viewState = {opening: 'opening', open: 'open', closing: 'closing', closed: 'closed'};
    const [view, setView] = useState(viewState.closed);

    return (
        <div className={classes.screen}>
            <div id={SCREENS.gifts} className={classes.position}/>
            <div className={classes.bigTitle}>Подарки</div>
            <div className={classes.subTitle}>в честь знакомства</div>
            <div className={classes.title}>Выгода</div>
            <div className={classes.title}>1500 ₽<sup>*</sup></div>
            <div className={classes.subTitle}>на первую замену масла</div>
            <div className={classes.subSubTitle}><sup>*</sup>в зависимости от марки автомобиля</div>
            <Button styles={[ButtonStyles.white]} handler={() => {
                setView(viewState.opening);
                setTimeout(() => setView(viewState.open), 1);
            }}>ЗАГЛЯНУТЬ</Button>
            <div className={classes.gift}>
                <div className={classes.text}/>
                <div className={classes.box}/>
            </div>
            <>
                <div className={classes.shadow + ' ' + classes[view]}/>
                <div className={cx({[classes.showGifts]: true, [classes[view]]: true})}>
                    <GiftForDating gift={giftForDating} close={() => setView(viewState.closing)}/>
                    {/*<Close className={classes.closePosition} styles={[CloseStyles.white]} handler={() => {
                        setView(viewState.closing);
                        setTimeout(() => setView(viewState.closed), 400);
                    }}/>
                    <div className={classes.text}>
                        При первой замене масла <span>вы получаете:</span>
                    </div>
                    <div className={classes.gifts}>
                        <div className={classes.gift}>
                            <div className={classes.icon}>-600р</div>
                            <div className={classes.text}>
                                Бесплатно услугу
                            </div>
                        </div>
                        <div className={classes.gift}>
                            <div className={classes.icon + ' ' + classes.heart}/>
                            <div className={classes.text}>
                                Сервис - Бизнес класса
                            </div>
                        </div>
                        <div className={classes.gift}>
                            <div className={classes.icon}>-600р</div>
                            <div className={classes.text}>Визуальный осмотр ходовой части по чек-листу</div>
                        </div>
                        <div className={classes.gift + ' ' + classes.dark}>
                            <div className={classes.icon}>-5%</div>
                            <div className={classes.text}>5 % скидка на весь заказ при онлайн-записи</div>
                        </div>
                    </div>
                    <div className={classes.buttonPosition}>
                        <Button styles={[ButtonStyles.blue, ButtonStyles.fullWidth]}>Получить</Button>
                    </div>*/}

                </div>
            </>

        </div>
    );
}

export default GiftSections;