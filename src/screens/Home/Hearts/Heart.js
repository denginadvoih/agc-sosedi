import React from 'react';
import classes from './heart.module.scss';

const Heart = ({ title, icon }) => {
    return (<div className={classes.heart}>

        <div className={classes.icon}>
            <img src={icon} alt=""/>
        </div>
        <div className={classes.title}>{title}</div>
    </div>);
}
export default Heart;