import React, { useEffect, useLayoutEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Swiper, SwiperSlide, useSwiper } from 'swiper/react';
// Import Swiper styles
import 'swiper/css';
import 'swiper/scss/pagination';
import classes from './hearts.module.scss'
import { SCREENS } from '../../../utils/constants';
import Heart from './Heart';

const Hearts = ({ hearts }) => {

    return <>
        <div className={classes.screen} id="heartId">
            <div id={SCREENS.hearts}/>
            <div className={classes.hearts} id="heartsSwiper">
                <Swiper
                    slidesPerView={2}
                    loop
                >
                    {
                        hearts.map((heart, key) => <SwiperSlide key={key}><Heart {...heart} /></SwiperSlide>)
                    }
                </Swiper>
            </div>
        </div>
    </>;
}

Hearts.propTypes = {
    hearts: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        icon: PropTypes.string
    }))
}


export default Hearts;