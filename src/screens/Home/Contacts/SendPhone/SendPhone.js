import React, { useRef, useState } from 'react';
import ReCAPTCHA from 'react-google-recaptcha';
import classes from './sendPhone.module.scss';
import InputMask from 'react-input-mask';
import Button from 'components/Button';
import { ButtonStyles } from 'components/Button/Button';
import { FETCH_RESULT, RECAPTCHA_KEY } from 'utils/constants';

const SendPhone = ({ mailIsSent, sendMail, sendMailReset }) => {
    const [phone, setPhone] = useState('');
    const [name, setName] = useState('');
    const [car, setCar] = useState('');
    const [privateAgree, setPrivateAgree] = useState(false);
    const reCaptchaRef = useRef();

    const sendData = async () => {
        const token = await reCaptchaRef.current.executeAsync();
        console.log(token);
        const data = {
            phone: phone,
            name: name,
            token: token
        };
        sendMail(data);
    }

    const validData = () => {
        return name !== '' && phone.replace(/[^+\d]/g, '').length === 12
    }


    return <div className={classes.sendPhone}>
        <div className={classes.header}>
            <div>Давайте знакомиться</div>
            <div>Подарок при первой записи</div>
        </div>
        <div className={classes.input}>
            <label>Ваше имя</label>
            <input placeholder={''} type="text" value={name}
                   onChange={(e) => setName(e.target.value)}/>
        </div>
        <div className={classes.input}>
            <label>Телефон</label>
            <InputMask placeholder={'+7 000 000 00 00'} value={phone} mask="+7 999 999 99 99"
                       onChange={(e) => setPhone(e.target.value)}/>
        </div>
        <div className={classes.input}>
            <label>Марка автомобиля</label>
            <input placeholder={''} type="text" value={car}
                   onChange={(e) => setCar(e.target.value)}/>
        </div>
        <div className={classes.privateAgree}>
            <label>
                <input type={'checkbox'} value={privateAgree}
                       checked={privateAgree}
                       onChange={() => setPrivateAgree((privateAgree) => !privateAgree)}/>
                <div/>
                <div className={classes.text}>Я согласен с политикой конфиденциальности</div>
            </label>
        </div>
        <div className={classes.button}>
            <Button handler={() => sendData()}
                    disabled={!validData()}
                    styles={[ButtonStyles.blue, ButtonStyles.fullWidth]}>Отправить</Button>
        </div>

        <ReCAPTCHA
            sitekey={RECAPTCHA_KEY}
            size="invisible"
            ref={reCaptchaRef}
        />
        {mailIsSent === FETCH_RESULT.success
            ?
            <div className={classes.shadowWrapper} onClick={(e) => {
                e.stopPropagation();
                sendMailReset();
            }}>
                <div className={classes.modal} onClick={(e) => {
                    e.stopPropagation();
                    sendMailReset();
                }}>
                    <div className={classes.button}/>
                    <div className={classes.header}>Спасибо за обращение!</div>
                    <div className={classes.text}>В ближайшее время мы с вами свяжемся</div>
                </div>
            </div>
            :
            null
        }
    </div>
}
export default SendPhone;