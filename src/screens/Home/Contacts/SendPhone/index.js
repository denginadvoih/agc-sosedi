import {connect} from "react-redux";
import SendPhone from './SendPhone.js';
import { sendMail, sendMailReset } from "redux/data";

const mapStateToProps = state => {
    return {
        mailIsSent: state.data.mailIsSent,
        fetching: state.data.fetching
    }
};

export default connect(mapStateToProps, { sendMail, sendMailReset })(SendPhone);