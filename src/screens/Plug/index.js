import {connect} from "react-redux";
import { changeStartStatus } from "redux/hangar";
import Plug from './Plug.js';

const mapStateToProps = state => {
    return {
        startStatus: state.hangar.startStatus
    }
};

export default connect(mapStateToProps, { changeStartStatus })(Plug);
